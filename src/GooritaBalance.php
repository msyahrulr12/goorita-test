<?php

namespace Goorita\Service;

use App\Entities\Balance;
use Illuminate\Support\Facades\Auth;

class GooritaBalance {
    public static function getBalance(array $data)
    {
        $data = array_merge([
            'user_id' => Auth::user()->id,
            'category' => 'SALDO'
        ], $data);
        return $data;
        $balance = Balance::where($data)->where('status', '<>', 99)->orderBy('created_at', 'DESC')->first();
        return ($balance) ? $balance->saldo : 0;
    }
}