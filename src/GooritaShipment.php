<?php

namespace Goorita\Service;

use GuzzleHttp\Client;
use App\Entities\Balance;
use App\Entities\Shipment;
use Illuminate\Support\Str;
use App\Entities\UserAddress;
use App\Entities\ItemCategory;
use App\Entities\ShipmentItem;
use App\Entities\ShipmentRoute;
use App\Entities\ShipmentHistory;
use App\Entities\ShipmentPayment;
use App\Services\ShipmentService;
use Illuminate\Support\Facades\Auth;
use GDGTangier\PubSub\Publisher\Facades\PublisherFacade;


class GooritaShipment {
    public $route;
    public function __construct()
    {
        $this->route = [];
        $this->last_route = [];   
        $this->master = 0; 
    }

    public static function getInfo($request)
    {
        $weight = 0;
        $total_weight = 0;
        $item_count = 0;
        $item_variant_count = 0;
        $oversize = 0;
        $overweight = 0;
        $oversizeLimit = env('OVERSIZE_LIMIT', 110);
        $overweightLimit = env('OVERWEIGHT_LIMIT', 65);
        $oversizePrice = env('OVERSIZE_PRICE', 1300000);
        $overweightPrice = env('OVERWEIGHT_PRICE', 1300000);
        $fulfillmentFee = 0;
        $depositFee = 0;
        if (is_array($request->weight)) {
            foreach ($request->weight as $key => $value) {
                $qty = data_get($request, 'qty.'.$key, 1);
                $item_variant_count++;
                $item_count += $qty;
                $rawVolume = ($request->length[$key] * $request->height[$key] * $request->width[$key] / 5000);
                $volume = $rawVolume * $qty;
                if ($request->length[$key] > $oversizeLimit ||  $request->height[$key] > $oversizeLimit || $request->width[$key] > $oversizeLimit) {
                    $oversize += $oversizePrice;
                }
                if (($request->weight[$key] * $qty) > $overweightLimit) {
                    $overweight += $overweightPrice;
                }
                $rawWeight = $request->weight[$key];
                $weight =  $rawWeight * $qty;
                $weight = ($weight > $volume) ? $weight : $volume;
                $rawWeight = ($rawWeight > $rawVolume) ? $rawWeight : $rawVolume;
                $total_weight += $weight;

                if ($weight > 5) {
                    $fulfillmentFee += 3.52 * env('KURS', 15000);
                } else if ($weight > 3) {
                    $fulfillmentFee += 3 * env('KURS', 15000);
                } else if ($weight > 1) {
                    $fulfillmentFee += 2.5 * env('KURS', 15000);
                } else if ($weight > 0.5) {
                    $fulfillmentFee += 2.25 * env('KURS', 15000);
                } else {
                    $fulfillmentFee += 2 * env('KURS', 15000);
                }

                if ($rawWeight > 5) {
                    $depositFee += 1.70 * env('KURS', 15000) * $qty * 6;
                } else if ($rawWeight > 3) {
                    $depositFee += 0.9 * env('KURS', 15000) * $qty * 6;
                } else if ($rawWeight > 1) {
                    $depositFee += 0.5 * env('KURS', 15000) * $qty * 6;
                } else if ($rawWeight > 0.5) {
                    $depositFee += 0.2 * env('KURS', 15000) * $qty * 6;
                } else {
                    $depositFee += 0.10 * env('KURS', 15000) * $qty * 6;
                }
            }
        } else {
            $item_variant_count = 1;
            $qty = data_get($request, 'qty', 1);
            $item_count = $qty;
            $volume = ($request->length * $request->height * $request->width / 5000) * $qty;
            if ($request->length > $oversizeLimit ||  $request->height > $oversizeLimit || $request->width > $oversizeLimit) {
                $oversize += $oversizePrice;
            }
            if (($request->weight * $qty) > $overweightLimit) {
                $overweight += $overweightPrice;
            }
            $weight = ($request->weight > $volume) ? $request->weight : $volume; 
            $weight = $weight * $qty;
            $total_weight += $weight;
        }
        return [
            'weight' => $total_weight, 
            'item_count' => $item_count, 
            'item_variant_count' => $item_variant_count, 
            'oversize' => $oversize, 
            'overweight'=> $overweight,
            'fulfillment_fee' => $fulfillmentFee,
            'deposit_fee' => $depositFee
        ];
    }

    public function getRoute($input, $route, $onlyOne=false, $data_before=[])
    {
        if ($onlyOne) {
            $data = $route['parent'];
            
            $data['from_name'] = $input['sender_name'];
            $data['from_country'] = $input['sender_country'];
            $data['from_country_code'] = $input['sender_country_code'];
            $data['from_address_line_1'] = $input['sender_address_line_1'];
            $data['from_address_line_2'] = $input['sender_address_line_2'];
            $data['from_province'] = $input['sender_province'];
            $data['from_city'] = $input['sender_city'];
            $data['from_zipcode'] = $input['sender_zipcode'];
            
            $data['to_name'] = $input['receiver_name'];
            $data['to_country'] = $input['receiver_country'];
            $data['to_country_code'] = $input['receiver_country_code'];
            $data['to_address_line_1'] = $input['receiver_address_line_1'];
            $data['to_address_line_2'] = $input['receiver_address_line_2'];
            $data['to_province'] = $input['receiver_province'];
            $data['to_city'] = $input['receiver_city'];
            $data['to_zipcode'] = $input['receiver_zipcode'];
            
            $data['courier_id'] = data_get($data, 'courier_id');
            $data['courier_code'] = data_get($data, 'code');
            $data['courier_name'] = data_get($data, 'name');
            $data['courier_service'] = data_get($data, 'courier_service');

            $route['parent'] = '';
        } else if (!empty($route['is_hub'])){
            if (empty($this->last_route)) {
                $data['from_name'] = $input['sender_name'];
                $data['from_country'] = $input['sender_country'];
                $data['from_country_code'] = $input['sender_country_code'];
                $data['from_address_line_1'] = $input['sender_address_line_1'];
                $data['from_address_line_2'] = $input['sender_address_line_2'];
                $data['from_province'] = $input['sender_province'];
                $data['from_city'] = $input['sender_city'];
                $data['from_zipcode'] = $input['sender_zipcode'];
            } else {
                $data['from_name'] = $this->last_route['to_name'];
                $data['from_country'] = $this->last_route['to_country'];
                $data['from_country_code'] = $this->last_route['to_country_code'];
                $data['from_address_line_1'] = $this->last_route['to_address_line_1'];
                $data['from_address_line_2'] = $this->last_route['to_address_line_2'];
                $data['from_province'] = $this->last_route['to_province'];
                $data['from_city'] = $this->last_route['to_city'];
                $data['from_zipcode'] = $this->last_route['to_zipcode'];
            }
            

            $data['to_name'] = data_get($route, 'name_pic');
            $data['to_country'] = data_get($route, 'country');
            $data['to_country_code'] = data_get($route, 'country_code');
            $data['to_address_line_1'] = data_get($route, 'address_line_1');
            $data['to_address_line_2'] = data_get($route, 'address_line_2');
            $data['to_province'] = data_get($route, 'province');
            $data['to_city'] = data_get($route, 'city');
            $data['to_zipcode'] = data_get($route, 'zipcode');
            $data['courier_hub_id'] = data_get($route, 'courier_hub_id');
            
            $data['courier_code'] = data_get($data_before, 'code');
            $data['courier_name'] = data_get($data_before, 'name');
            $data['courier_service'] = data_get($data_before, 'courier_service');
            $data['courier_id'] = data_get($data_before, 'courier_id');
            $data['fuel'] = data_get($data_before, 'fuel');
            $data['price'] = (double)data_get($data_before, 'price');
            $data['range_end'] = (int)data_get($data_before, 'range_end');
            $data['range_start'] = (int)data_get($data_before, 'range_start');
            $data['round_up'] = (double)data_get($data_before, 'round_up');
            $data['scheme'] = data_get($data_before, 'scheme');
            $data['sub_total_price'] = data_get($data_before, 'sub_total_price');
            $data['sum_sub_total_price'] = data_get($data_before, 'sum_sub_total_price');
            $data['sum_total_range_end'] = data_get($data_before, 'sum_total_range_end');
            $data['sum_total_range_start'] = data_get($data_before, 'sum_total_range_start');
            $data['tax'] = data_get($data_before, 'tax');
            $data['type'] = data_get($data_before, 'type');
            $data['zone_id'] = data_get($data_before, 'zone_id');
            $data['zone_name'] = data_get($data_before, 'zone_name');
            $data['zone_price_id'] = data_get($data_before, 'zone_price_id');
            $data['account_number'] = data_get($data_before, 'account_number');
        } else if ($route['type'] == 'lm') {
            $data['from_name'] = $this->last_route['to_name'];
            $data['from_country'] = $this->last_route['to_country'];
            $data['from_country_code'] = $this->last_route['to_country_code'];
            $data['from_address_line_1'] = $this->last_route['to_address_line_1'];
            $data['from_address_line_2'] = $this->last_route['to_address_line_2'];
            $data['from_province'] = $this->last_route['to_province'];
            $data['from_city'] = $this->last_route['to_city'];
            $data['from_zipcode'] = $this->last_route['to_zipcode'];

            $data['to_name'] = $input['receiver_name'];
            $data['to_country'] = $input['receiver_country'];
            $data['to_country_code'] = $input['receiver_country_code'];
            $data['to_address_line_1'] = $input['receiver_address_line_1'];
            $data['to_address_line_2'] = $input['receiver_address_line_2'];
            $data['to_province'] = $input['receiver_province'];
            $data['to_city'] = $input['receiver_city'];
            $data['to_zipcode'] = $input['receiver_zipcode'];

            $data['courier_code'] = data_get($route, 'code');
            $data['courier_name'] = data_get($route, 'name');
            $data['courier_service'] = data_get($route, 'courier_service');
            $data['courier_id'] = data_get($route, 'courier_id');
            $data['fuel'] = data_get($route, 'fuel');
            $data['price'] = (double)data_get($route, 'price');
            $data['range_end'] = (int)data_get($route, 'range_end');
            $data['range_start'] = (int)data_get($route, 'range_start');
            $data['round_up'] = (double)data_get($route, 'round_up');
            $data['scheme'] = data_get($route, 'scheme');
            $data['sub_total_price'] = data_get($route, 'sub_total_price');
            $data['sum_sub_total_price'] = data_get($route, 'sum_sub_total_price');
            $data['sum_total_range_end'] = data_get($route, 'sum_total_range_end');
            $data['sum_total_range_start'] = data_get($route, 'sum_total_range_start');
            $data['tax'] = data_get($route, 'tax');
            $data['type'] = data_get($route, 'type');
            $data['zone_id'] = data_get($route, 'zone_id');
            $data['zone_name'] = data_get($route, 'zone_name');
            $data['zone_price_id'] = data_get($route, 'zone_price_id');
            $data['account_number'] = data_get($route, 'account_number');
        }
        if (!empty($data)) {
            $data['status'] = 0;
            $data['shipment_id'] = $input['id'];
            if (!empty($route['is_hub'])) {
                if ($this->master == 0) {
                    data_fill($data, 'action', 'bulk');
                }

                if (data_get($route, 'parent.type') != 'm') {
                    data_fill($data, 'action', 'break-bulk');
                }
                $this->master += 1;
            }
            $this->route[] = $data;
            $this->last_route = $data;
        }
        
        if (!empty($route['parent'])) $this->getRoute($input, $route['parent'], $onlyOne, $route);
    }

    public static function calculateInsurance($price, $qty)
    {
        $insurance = 0;
        if (!is_array($price)) $price = []; 
        foreach ($price as $key => $value) {
            $percentage = $value * 2/100;
            $minimumCharge = 150000;
            if ($percentage > $minimumCharge) {
                $insurance += ($percentage * $qty[$key]);
            } else {
                $insurance += $minimumCharge * $qty[$key];
            }
        }
        return $insurance;
    }

    public static function saveItem($data, $id)
    {
        foreach ($data['length'] as $key => $value) {
            $oversize = 0;
            $overweight = 0;
            $oversizeLimit = env('OVERSIZE_LIMIT', 110);
            $overweightLimit = env('OVERWEIGHT_LIMIT', 65);
            $oversizePrice = env('OVERSIZE_PRICE', 1300000);
            $overweightPrice = env('OVERWEIGHT_PRICE', 1300000);
            if ($data['length'][$key] > $oversizeLimit ||  $data['height'][$key] > $oversizeLimit || $data['width'][$key] > $oversizeLimit) {
                $oversize = $oversizePrice;
            }
            if (($data['weight'][$key] * $data['qty'][$key]) > $overweightLimit) {
                $overweight = $overweightPrice;
            }
            $category = ItemCategory::selectedField()->with('parent')->find($data['category'][$key]);
            ShipmentItem::create([
                'length' => $data['length'][$key],
                'width' => $data['width'][$key],
                'height' => $data['height'][$key],
                'weight' => $data['weight'][$key],
                'name' => $data['description'][$key],
                'price' => $data['price'][$key],
                'qty' => $data['qty'][$key],
                'category_id' => $data['category'][$key],
                'category_name' => data_get($category, 'parent.name', '').' - '.data_get($category, 'name', ''),
                'category_data' => json_encode($category),
                'description' => $data['description'][$key],
                'shipment_id' => $id,
                'oversize_amount' => $oversize,
                'overweight_amount' => $overweight
            ]);
        }
    }

    public static function saveRoute($data, $route, $onlyOne)
    {
        $shipmentService = new ShipmentService();
        $shipmentService->getRoute($data, $route, $onlyOne);
        $routes = $shipmentService->route;
        $seq = 0;
        foreach ($routes as $key => $value) {
            $value['seq'] = $seq;
            $seq++;
            ShipmentRoute::create($value);
        }
    }

    public static function getReferenceCode()
    {
        $random = strtoupper(Str::random(5));
        $check = Shipment::where('reference_code', $random)->first();
        if ($check) {
            return self::getReferenceCode();
        } else {
            return $random;
        }
    }

    public static function savePayment($data, $shipmment_id)
    {
        $shipmentPayment = ShipmentPayment::create([
            'status' => 0,
            'method' => $data['payment_method'],
            'invoice_number' => data_get($data, 'invoice_number'),
            'invoice_url' => data_get($data, 'invoice_url'),
            'descriptionn' => 'Pembayaran Shipment '.$data['reference_code'],
            'amount' => $data['total_price'],
            'shipment_id' => $shipmment_id,
        ]);
        PublisherFacade::publish(json_encode(['id'=>$shipmment_id]), 'SHIPMENT_GENERATE_LABEL');
        if ($data['payment_method'] == 'saldo') {
            $client = new Client();
            $client->request('POST', env('URL_BALANCE').'payment/shipment', [
                'json' => [
                    'id' => $shipmentPayment->id
                ]
            ]);
        }
    }

    public static function saveHistory($data, $shipmment_id)
    {
        ShipmentHistory::create([
            'status' => 0,
            'description' => 'Pemesanan Pengiriman',
            'data' => json_encode($data),
            'shipment_id' => $shipmment_id,
        ]);
    }

    public static function checkSavedAddress($data)
    {
        $userId = auth()->user()->id;        
        if (!empty($data['save_sender_address'])) {
            $senderData = [
                "label" => $data['sender_label'],
                "address_line_1" => $data['sender_address_line_1'],
                "address_line_2" => $data['sender_address_line_2'],
                "country" => $data['sender_country'],
                "country_code" => $data['sender_country_code'],
                "zipcode" => $data['sender_zipcode'],
                "city" => $data['sender_city'],
                "province" => $data['sender_province'],
                "phone" => $data['sender_phone'],
                "email" => $data['sender_email'],
                "instruction" => $data['sender_instruction'],
                "user_id" => $userId,
                "name" => $data['sender_name']
            ];
            if (empty($data['sender_address_id'])) {
                UserAddress::create($senderData);
            } else {
                UserAddress::where("id", $data['sender_address_id'])->update($senderData);
            }
        }

        if (!empty($data['save_receiver_address'])) {
            $receiverData = [
                "label" => $data['receiver_label'],
                "address_line_1" => $data['receiver_address_line_1'],
                "address_line_2" => $data['receiver_address_line_2'],
                "country" => $data['receiver_country'],
                "country_code" => $data['receiver_country_code'],
                "zipcode" => $data['receiver_zipcode'],
                "city" => $data['receiver_city'],
                "province" => $data['receiver_province'],
                "phone" => $data['receiver_phone'],
                "email" => $data['receiver_email'],
                "instruction" => $data['receiver_instruction'],
                "user_id" => $userId,
                "name" => $data['receiver_name']
            ];
            if (empty($data['receiver_address_id'])) {
                UserAddress::create($receiverData);
            } else {
                UserAddress::where("id", $data['receiver_address_id'])->update($receiverData);
            }
        }

        if (!empty($data['save_pickup_address'])) {
            $pickupData = [
                "label" => $data['pickup_label'],
                "address_line_1" => $data['pickup_address_line_1'],
                "address_line_2" => $data['pickup_address_line_2'],
                "country" => $data['pickup_country'],
                "country_code" => $data['pickup_country_code'],
                "zipcode" => $data['pickup_zipcode'],
                "city" => $data['pickup_city'],
                "province" => $data['pickup_province'],
                "phone" => $data['pickup_phone'],
                "email" => $data['pickup_email'],
                "instruction" => $data['pickup_instruction'],
                "user_id" => $userId,
                "name" => $data['pickup_name']
            ];
            if (empty($data['pickup_address_id'])) {
                UserAddress::create($pickupData);
            } else {
                UserAddress::where("id", $data['pickup_address_id'])->update($pickupData);
            }
        }
    }
}